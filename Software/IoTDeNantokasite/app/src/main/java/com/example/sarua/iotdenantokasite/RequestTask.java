package com.example.sarua.iotdenantokasite;

/**
 * Created by sarua on 2018/01/15.
 */
import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class RequestTask extends AsyncTask<String, Void, String> {
    private Activity m_Activity;

    private double temperature;
    private boolean pcTurnOn = false;
    private String onOrOFF = "OFF";
    private double luminance;
    private double humidity;

    String mes ="";

    public RequestTask(Activity activity){
        // 呼び出し元のアクティビティを変数へセット
        this.m_Activity=activity;
    }

    @Override
    protected String doInBackground(String... params) {
        //"temperature":"25.0","pcTurnOn":"true","luminance":"114514"}//typeがstatu


        try {
            Socket socket =
                    //new Socket("10.0.2.2",5003);  // 接続開始
                    new Socket("192.168.0.13"/*serverName*/, 10000);
                    mes = "";
            Log.d("Request Task","サーバーからの接続に成功");
            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            BufferedReader reader2 = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String requestJsonStr="{\"type\":\"request\",\"content\":{}}";
            writer.println(requestJsonStr);
            writer.flush();
            String result = reader2.readLine();
            Log.d("Request Task",result);

            //JSON解読
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree( result);
            temperature = Double.parseDouble(root.get("content").get("temperature").asText());
            pcTurnOn = root.get("content").get("pcTurnOn").asBoolean();
            luminance = Double.parseDouble(root.get("content").get("luminance").asText());
            humidity = Double.parseDouble(root.get("content").get("humidity").asText());
            Log.d("RequestTask",temperature+" , "+ pcTurnOn+" , "+luminance);


            //すべて閉じる
            reader2.close();
            writer.close();
            socket.close();


        } catch (IOException e) {
            e.printStackTrace();
            mes = "サーバーからの接続に失敗";
            Log.d("Request Task","サーバーからの接続に失敗    IO Exception");


        }



        return null;
    }


    @Override
    protected void onPostExecute(String param) {

        //ボタンを宣言
        Button button1 = (Button)this.m_Activity.findViewById(R.id.button_light);//ライト
        Button button2 = (Button)this.m_Activity.findViewById(R.id.button_pc);//PC
        Button button3 = (Button)this.m_Activity.findViewById(R.id.button_airconOn);//エアコン
        Button button4 = (Button)this.m_Activity.findViewById(R.id.button_airconOff);//エアコン

        // buttonの文字列をセット
        button1.setText("照明  輝度 "+luminance);
        if(pcTurnOn){
            onOrOFF ="ON";
        }else{
            onOrOFF ="OFF";
        }
        button2.setText("PC   "+onOrOFF);
        button3.setText("エアコンON  気温："+temperature+"℃");
        button4.setText("エアコンOFF  湿度："+humidity+"%");

        //メッセージテキストを更新
        TextView textView = (TextView)this.m_Activity.findViewById(R.id.messageText);
        textView.setText(mes);


        return;
    }
}
