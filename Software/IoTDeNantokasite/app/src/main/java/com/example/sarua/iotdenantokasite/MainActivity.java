package com.example.sarua.iotdenantokasite;



        import android.app.Activity;
        import android.graphics.Typeface;
        import android.os.Bundle;
        import android.os.Handler;
        import android.util.Log;
        import android.view.View;
        import android.view.Window;
        import android.widget.Button;
        import android.widget.TextView;
        import android.widget.Toast;

        import java.util.Timer;
        import java.util.TimerTask;


public class MainActivity extends Activity {
    Timer timer;
    Activity  activity = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //タイトルバーを非表示にする（必ずsetContentViewの前にすること）
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // レイアウトをセットする
        setContentView(R.layout.activity_main);
        //接続
        //TCPTask tcpTask = new TCPTask();
        //tcpTask.execute();
        RequestTask requestTask = new RequestTask(this);
        requestTask.execute();
        //TcpTest tt = new TcpTest();
        //tt.start();
        timer = new Timer();
        Log.d("main","レイアウト生成");

        timer.scheduleAtFixedRate(
                new TimerTask()
                {
                    @Override
                    public void run()
                    {
                        Log.d("main", "定期処理");
                        RequestTask requestTask = new RequestTask(activity);
                        requestTask.execute();
                    }
                }, 10, 2000);
        //10ミリ秒後に100ミリ秒間隔でタスク実行

        // フォントを変更 ここから***********************************************************************************

        // フォントを変更 ここまで***********************************************************************************
    }

    // ボタンがタッチされた時の処理
    public void onClick(View v){
        switch (v.getId()) {
            // タッチされたボタンが照明の場合
            case R.id.button_light:
                Toast.makeText(this, "照明をスイッチします", Toast.LENGTH_SHORT).show();
                CommandTask commandLight = new CommandTask(this,0);
                commandLight.execute();
                break;
            // タッチされたボタンがPCの場合
            case R.id.button_pc:
                Toast.makeText(this, "PCの電源ボタンを押します", Toast.LENGTH_SHORT).show();
                CommandTask commmandPc = new CommandTask(this,1);
                commmandPc.execute();
                break;
            // タッチされたボタンがエアコンの場合
            case R.id.button_airconOn:
                Toast.makeText(this, "エアコンをONにします", Toast.LENGTH_SHORT).show();
                CommandTask commmandAirConOn = new CommandTask(this,2);
                commmandAirConOn.execute();
                break;
            case R.id.button_airconOff:
                Toast.makeText(this, "エアコンをOFFにします", Toast.LENGTH_SHORT).show();
                CommandTask commmandAirConOff = new CommandTask(this,3);
                commmandAirConOff.execute();
                break;

        }
    }
}