//温度センサ
#include <Arduino.h>
#include <Wire.h>
#include "SHT31.h"
SHT31 sht31 = SHT31();
//照度センサ
#include <math.h>
#define LIGHT_SENSOR A6//Grove - Light Sensor is connected to A0 of Arduino
const int thresholdvalue = 10;       //The treshold for which the LED should turn on. Setting it lower will make it go on at more light, higher for more darkness

//IRリモコン
#define IR_LED_PIN  19
#define PULSE_DELAY 8
//unsigned int irData[] = {343, 159, 48, 120, 48, 33, 50, 33, 50, 35, 48, 120, 48, 120, 48, 36, 48, 33, 50, 120, 48, 120, 48, 120, 48, 33, 50, 33, 50, 120, 48, 120, 48, 36, 48, 120, 48, 120, 48, 36, 48, 33, 51, 36, 48, 33, 50, 33, 50, 33, 50, 36, 48, 120, 48, 36, 48, 36, 48, 36, 48, 36, 48, 36, 48, 36, 48, 33, 50, 120, 48, 36, 48, 120, 48, 120, 48, 36, 48, 120, 48, 36, 48, 33, 50, 36, 48, 36, 48, 120, 48, 120, 48, 36, 48, 120, 48, 36, 48, 13881, 343, 160, 48, 120, 48, 36, 47, 36, 48, 36, 48, 120, 48, 120, 48, 36, 48, 36, 48, 120, 48, 120, 48, 120, 48, 36, 47, 36, 48, 120, 48, 120, 48, 36, 48, 120, 48, 120, 48, 36, 48, 36, 48, 33, 50, 35, 48, 36, 48, 36, 48, 36, 48, 120, 48, 35, 48, 35, 48, 36, 48, 36, 48, 36, 48, 36, 48, 33, 50, 120, 48, 36, 48, 120, 48, 120, 48, 33, 50, 120, 48, 36, 48, 36, 48, 36, 48, 36, 48, 120, 48, 120, 48, 36, 48, 120, 48, 36, 48};
unsigned int data1[] = {211,96,568,98,158,46,158,46,56,46,56,46,56,46,55,46,56,46,56,46,158,45,55,46,56,46,56,46,56,46,56,46,56,46,158,46,55,46,158,46,55,46,56,46,56,46,56,46,56,46,55,46,55,46,55,46,55,46,55,46,55,46,56,46,56,46,55,46,56,46,158,46,56,46,158,46,158,46,56,46,158,46,158,44,58};
unsigned int data2[] = {335,157,48,33,50,36,47,118,48,33,50,117,48,33,50,33,50,33,50,117,48,117,47,33,50,33,50,33,50,117,48,117,48,33,50,33,50,36,48,33,50,33,50,33,50,33,50,33,50,33,50,33,50,33,50,33,50,33,50,117,48,33,50,33,50,33,50,33,50,33,50,33,50,33,50,117,49,32,50,36,48,33,50,33,50,33,51,117,48,117,48,117,48,117,48,117,48,117,48,33,50,35,48,33,50,117,48,33,50,33,50,33,50,33,50,33,50,36,48,36,48,36,48,117,48,117,48,36,48,33,50,117,48,33,50,36,48,33,50,117,48,33,50,117,48,36,48,36,48,35,48,117,48,33,50,33,50,36,48,36,48,33,50,33,50,36,48,33,50,36,48,117,48,36,48,36,47,36,48,36,48,36,48,33,50,33,50,36,48,35,48,33,50,36,48,33,50,36,48,36,48,36,48,33,50,33,50,36,48,33,50,36,48,36,48,36,48,35,48,36,48,33,50,36,48,36,48,117,48,117,48,36,47,118,48,36,48,117,48,117,48,33,50};
unsigned int data3[] = {335,157,48,35,48,36,48,117,48,36,48,117,48,36,48,36,48,33,50,117,48,117,48,33,50,36,48,36,48,117,48,117,47,36,48,36,48,33,50,36,48,36,48,33,50,36,48,36,48,36,48,36,48,36,48,33,50,36,48,117,48,36,48,36,48,36,48,36,48,36,48,36,48,36,47,117,47,36,48,36,48,36,48,36,48,117,47,36,48,36,48,36,47,36,45,38,45,38,45};


//wifi周り
#include <WiFi.h>
#include <ArduinoJson.h>
const char* ssid     = "futaba-chan";
const char* password = "sansyasanyou";

WiFiServer server(10000);

void setup()
{
  Serial.begin(115200);

  delay(10);
  sht31.begin();
  pinMode(4, INPUT_PULLUP);
  pinMode(5, OUTPUT);
  pinMode(IR_LED_PIN,  OUTPUT);

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}
/*TCPサーバの待ち受け、データを受け取ったらexecuteに投げ、戻ってきたjson(string)を返す*/
void loop() {
  //Serial.println("loop start");
  WiFiClient client = server.available();
  if (client) {
    Serial.println("new client");
    while (client.connected()) {
      String currentLine = "";
      while (client.available()) {
        currentLine += client.readStringUntil('\r');
      }
      // Serial.println("bef execute");
      if (currentLine.length() > 0)
        client.println(execute(currentLine));
    }
    Serial.println("client bef disonnected");
    client.stop();
    Serial.println("client disonnected");
  }

}
/*温度くれ*/
float getTemp() {
  return sht31.getTemperature();
}
/*湿度くれ*/
float getHumi(){
  return sht31.getHumidity();
}
/*輝度くれ*/
float getLuminance() {
  return analogRead(LIGHT_SENSOR);
}
/*電源ONOFFくれ*/
bool getPcTurnOn() {
  Serial.println("getPcTurnOn");
  return digitalRead(4) == HIGH;
}
/*jsonをパースして、内容によって各種機能を実行*/
String execute(String payload) {
  Serial.println("execute");
  Serial.println(payload);
  String ret = "";
  DynamicJsonBuffer jsonBuffer(8000);
  JsonObject& root = jsonBuffer.parseObject(payload);
  if (!root.success()) {
    ret = "parse failed";
  }
  else {
    String type = root.get<String>("type");
    String content = root.get<String>("content");
    if (type == "request") {
      ret = "{\"type\":\"status\",\"content\":{" + getSensorsData() + "}}";
    }
    else if (type == "command") {
      Serial.println("start command");

      JsonObject& contentRoot = jsonBuffer.parseObject(content);
      pushSwitch(contentRoot.get<String>("switch"));
      ret = "{\"type\":\"status\",\"content\":{" + getSensorsData() + "}}";

      Serial.println("end command");
    }
    else {
      ret = "invalid json";
    }

  }
  Serial.println("return execute");
  return ret;
}
void pushSwitch(String target) {
  Serial.println("pushswitch");
  if (target == "pc")pushPcSwitch();
  else if (target == "light")pushLightSwitch();
  else if (target == "airConOn")pushAirConOnSwitch();
  else if (target == "airConOff")pushAirConOffSwitch();
  Serial.println("endpushswitch");
}
/*各種センサーの情報*/
String getSensorsData() {
  String ret = "\"temperature\":\"";
  ret += getTemp();
  ret += "\",\"humidity\":\"";
  ret += getHumi();
  ret += "\",\"pcTurnOn\":\"";
  ret += getPcTurnOn() ? "true" : "false";
  ret += "\",\"luminance\":\"";
  ret += getLuminance();
  ret += "\"";
  return ret;
}
/*各種スイッチの操作*/
void pushPcSwitch() {
  Serial.println("pushpcswitch");
  digitalWrite(5, HIGH);
  Serial.println("HIGH");
  delay(getPcTurnOn()?5000:1000);
  digitalWrite(5, LOW);
  Serial.println("LOW");
  Serial.println("endpushpcswitch");
}
void pushLightSwitch() {
  Serial.println("Send");

  unsigned int data1Size = sizeof(data1) / sizeof(data1[0]);
  for (unsigned int i = 0; i < data1Size; i++) {
    unsigned long length = data1[i] * 10;
    if (i % 2 == 0) {
      pulseOn(length);
    } else {
      pulseOff(length);
    }
  }
  delay(500);
}
void pushAirConOnSwitch() {
  Serial.println("Send");

  unsigned int data2Size = sizeof(data2) / sizeof(data2[0]);
  for (unsigned int i = 0; i < data2Size; i++) {
    unsigned long length = data2[i] * 10;
    if (i % 2 == 0) {
      pulseOn(length);
    } else {
      pulseOff(length);
    }
  }
  delay(500);
}
void pushAirConOffSwitch() {
  Serial.println("Send");

  unsigned int data3Size = sizeof(data3) / sizeof(data3[0]);
  for (unsigned int i = 0; i < data3Size; i++) {
    unsigned long length = data3[i] * 10;
    if (i % 2 == 0) {
      pulseOn(length);
    } else {
      pulseOff(length);
    }
  }
  delay(500);
}

void pulseOn (unsigned long length) {
  unsigned long startTime = micros();
  unsigned int flag = LOW;

  while ((micros() - startTime) < length) {
    digitalWrite(IR_LED_PIN, flag = !flag);
    delayMicroseconds(PULSE_DELAY);
  }
}

void pulseOff (unsigned long length) {
  digitalWrite(IR_LED_PIN, LOW);
  delayMicroseconds(length);
}



